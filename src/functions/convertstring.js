const convertToThai = (inputString) => {
    const englishToThai = {
        'a': 'ฟ',
        'b': 'ิ',
        'c': 'แ',
        'd': 'ก',
        'e': 'ำ',
        'f': 'ด',
        'g': 'เ',
        'h': '้',
        'i': 'ร',
        'j': '่',
        'k': 'า',
        'l': 'ส',
        'm': 'ท',
        'n': 'ื',
        'o': 'น',
        'p': 'ย',
        'q': 'ๆ',
        'r': 'พ',
        's': 'ห',
        't': 'ะ',
        'u': 'ี',
        'v': 'อ',
        'w': 'ไ',
        'x': 'ป',
        'y': 'ั',
        'z': 'ผ',
        ';': 'ว',
        ':': 'ซ',
        ',': 'ม',
        '.': 'ใ',
        '\/': 'ฝ',
        '!': '+',
        '@': '๑',
        '#': '๒',
        '$': '๓',
        '%': '๔',
        '^': 'ู',
        '&': '฿',
        '*': '๕',
        '(': '๖',
        ')': '๗',
        '-': 'ข',
        '_': '๘',
        '=': 'ช',
        '+': '๙',
        'A': 'ฤ',
        'B': 'ฺ',
        'C': 'ฉ',
        'D': 'ฏ',
        'E': 'ฎ',
        'F': 'โ',
        'G': 'ฌ',
        'H': '็',
        'I': 'ณ',
        'J': '๋',
        'K': 'ษ',
        'L': 'ศ',
        'M': '?',
        'N': '์',
        'O': 'ฯ',
        'P': 'ญ',
        'Q': '๐',
        'R': 'ฑ',
        'S': 'ฆ',
        'T': 'ธ',
        'U': '๊',
        'V': 'ฮ',
        'W': '"',
        'X': ')',
        'Y': 'ํ',
        'Z': '(',
        '0': 'จ',
        '1': 'ๅ',
        '2': '\/',
        '3': '-',
        '4': 'ภ',
        '5': 'ถ',
        '6': 'ุ',
        '7': 'ึ',
        '8': 'ค',
        '9': 'ต',
        '[': 'บ',
        ']': 'ล',
        '{': 'ฐ',
        '}': ',',
        '\'': 'ง', 
        '\"': '.',
        '<': 'ฒ',
        '>': 'ฬ'
        
    };      
    
    let thaiString = '';
    for (let i = 0; i < inputString.length; i++) {
      const englishChar = inputString.charAt(i).toLowerCase();
      const thaiChar = englishToThai[englishChar] || englishChar;
      thaiString += thaiChar;
    }
    return thaiString;
  }

const convertStringToAscii = ( string ) => {
  return string.charCodeAt(0);
}
  
export default {
  convertToThai,
  convertStringToAscii
}