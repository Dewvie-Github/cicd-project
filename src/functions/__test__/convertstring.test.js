import convertstring from "../convertstring"

test('converts English text to Thai text', () => {
  const inputString = 'l;ylfu8iy['
  const expectedOutput = 'สวัสดีครับ'

  expect(convertstring.convertToThai(inputString)).toBe(expectedOutput);
})


test('Convert input string to ascii', () => {
    expect( convertstring.convertStringToAscii("ร") )
    .toBe( "ร".charCodeAt(0) )
})
