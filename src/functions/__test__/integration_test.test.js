import calculate from "../calculate";
import convertstring from "../convertstring";


function mock_convertstring_convertToThai(ch) {
    if (ch == "i") {
        return "ร"
    }
}


test('Square root of charcode(i)', () => {
    // expect( calculate.squareRoot( mock_convertstring_convertToThai("i") ) )
    // .toBe( Math.sqrt( "ร".charCodeAt(0)) );

    expect( calculate.squareRoot( 
        convertstring.convertStringToAscii(
            convertstring.convertToThai("i")
            ) 
        ))
    .toBe( Math.sqrt( "ร".charCodeAt(0)) );
})
