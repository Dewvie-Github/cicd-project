import calculate from "../calculate";
import convertstring from "../convertstring";

test('Square root of 4 is 2', () => {
  expect(calculate.squareRoot(4)).toBe(2);
})

test(' Squareroot of convertstring.convertStringToAscii(\"ร\")', () => {
  let data 
  let result

  if (convertstring.convertToThai("i") == null &&
      convertstring.convertToThai("i") != "ร"
  ){
    data = "ร".charCodeAt(0)
    result = Math.sqrt(data)
  }else{
    data = convertstring.convertStringToAscii( convertstring.convertToThai("i") )
    // @ts-ignore
    result = Math.sqrt(data)
  }

  expect(calculate.squareRoot(data)).toBe(result);
})
